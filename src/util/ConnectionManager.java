package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionManager {

    public static final String URL = "db.url";
    public static final String USERNAME = "db.username";
    public static final String PASSWORD = "db.password";

    static {
        loadDriver();
    }

    public static Connection open(){
        try {
            return DriverManager.getConnection(
                    PropertyUtil.get(URL),
                    PropertyUtil.get(USERNAME),
                    PropertyUtil.get(PASSWORD));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private ConnectionManager(){
    }
}
