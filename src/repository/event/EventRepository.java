package repository.event;

import entiti.event.Event;


import java.util.List;

public interface EventRepository<T> {
    List<T> readAll();

    boolean edit();

    boolean create();

    Event read(int x);

    boolean remove();

    void removeOld(List<Integer> x);

    List<T> readOld();
}