package entiti.user;

public class Admin extends Person{

    public Admin(String login, String password) {
        super(login, password);
    }

    public Admin(Integer id, String login) {
        super(id, login);
    }

    public Admin(Integer id, String login, String password) {
        super(id, login, password);
    }


    class SubManager extends Manager{

        public SubManager(String login, String password) {
            super(login, password);
        }

        public SubManager(Integer id, String login) {
            super(id, login);
        }

        public SubManager(Integer id, String login, String password) {
            super(id, login, password);
        }
    }
}