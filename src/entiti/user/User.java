package entiti.user;

public class User extends Person{

    public User(String login, String password) {

        super(login, password);
    }

    public User(Integer id, String login) {

        super(id, login);
    }

    public User(Integer id, String login, String password) {

        super(id, login, password);
    }

    public User(String login, String password, int role) {
        super(login, password, role);
    }


    public User(int id, String login, String password, int role) {
        super(login, password, role);
    }


    public User() {

    }

}