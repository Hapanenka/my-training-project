package service.person;

import entiti.user.Person;
import repository.person.PersonRepository;

public class ServicePersonImpl implements ServicePerson {

    private PersonRepository<Person> personRepository;

    public ServicePersonImpl(PersonRepository<Person> personRepository) {
        this.personRepository = personRepository;
    }


    @Override
    public String registrationLoginUser() {
        try {
            return personRepository.registrationLogin();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String registrationPasswordUser() {
        try {
            return personRepository.registrationPassword();
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public Object enterUser() {
        try {
            return personRepository.enter();
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public void registrationUser(String t, String k) {
        try {
            personRepository.registration(t, k);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean removeUser() {
        try {
            return personRepository.remove();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Object readUserFromDB(Object o) {
        try {
            return personRepository.readFromDB((Person) o);
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public void changePasswordForUser() {
        try {
            personRepository.changePasswordFor();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeMinePassword(int i) {
        try {
            personRepository.changePassword(i);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeLoginForUser() {
        try {
            personRepository.changeLoginFor();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int readYourIdSession(Object o) {
        try {
            return personRepository.readIdSession((Person) o);
        }catch (Exception e) {
            return 0;
        }
    }

    @Override
    public String readYourName(int x) {
        try {
            return personRepository.readYour(x);
        }catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean comparisonPassword(Object o, Object k) {
        try {
            return personRepository.checkPassword((Person) o, (Person) k);
        }catch (Exception e) {
            return false;
        }

    }
}
