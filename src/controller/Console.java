package controller;

import entiti.event.Event;
import entiti.user.Person;
import entiti.user.User;
import service.event.ServiceEvent;
import service.person.ServicePerson;
import service.ticket.ServiceTicket;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Console implements ViewConsole {

    public static final Logger log = Logger.getLogger(Console.class.getSimpleName());

    int YOUR_ID;

    public ServicePerson readerPerson;
    public ServiceEvent readerEvent;
    public ServiceTicket readerTicket;

    public Console(ServicePerson readerPerson, ServiceEvent readerEvent, ServiceTicket readerTicket) {
        this.readerEvent = readerEvent;
        this.readerPerson = readerPerson;
        this.readerTicket = readerTicket;
    }

    @Override
    public int start() {
        try {
            while (true) {
                Scanner sc = new Scanner(System.in);
                System.out.println(" " + "\n" +
                        "===  ДОБРОПОЖАЛОВАТЬ В УЧЕБНЫЙ ПРОЕКТ 'КИНОТЕАТР'  ===" + "\n" +
                        " " + "\n" +
                        "1. Для входа введите '1'" + "\n" +
                        "2. Для регистрации нового пользователя введите '2'" + "\n" +
                        "0. Для выхода из программы введите '0'" + "\n" +
                        " " + "\n" +
                        "--->");
                String choice = sc.nextLine();
                switch (choice) {
                    case "0":
                        log.info("Завершение сеанса");
                        System.exit(1);
                    case "1":
                        log.info("Выбрано меню входа");
                        User firstUser = (User) readerPerson.enterUser();
                        User secondUser = (User) readerPerson.readUserFromDB(firstUser);
                        YOUR_ID = readerPerson.readYourIdSession(secondUser);
                        comparisonUsers(firstUser, secondUser);
                        break;
                    case "2":
                        log.info("Выбрано меню регистрации нового пользователя");
                        readerPerson.registrationUser(readerPerson.registrationLoginUser(), readerPerson.registrationPasswordUser());
                        break;
                    default:
                        System.err.println("Выбран неправильный пункт меню, повторите ввод");
                }
            }
        } catch (Exception e) {
            log.warning("Введены не коректные данные");
            System.out.println("ВЫ ВВЕЛИ НЕ КОРЕКТНЫЕ ДАННЫЕ" + "\n" +
                    "ПОВТОРИТЕ ПОПЫТКУ" + "\n" +
                    "================================================================");
            start();
        }
        return YOUR_ID;
    }

    @Override
    public void userMenu() {
        try {
            while (true) {
                Scanner sc = new Scanner(System.in);
                log.info("Открыто меню пользователя");
                System.out.println(" " + "\n" +
                        "======= МЕНЮ ПОЛЬЗОВАТЕЛЯ ========" + "\n" +
                        " " + "\n" +
                        "Выберете пункт меню:" + "\n" +
                        "1. Для просмотра мероприятий введите '1'" + "\n" +
                        "2. Для покупки билета введите '2'" + "\n" +
                        "3. Для просмотра ваших билетов введите '3'" + "\n" +
                        "4. Для возврата билета введите '4'" + "\n" +
                        "5. Для смены своего пароля введите '5'" + "\n" +
                        "9. Для возврата к предыдущему меню '9'" + "\n" +
                        "0. Для выхода из программы '0'" + "\n" +
                        "--->");
                String choice = sc.nextLine();
                switch (choice) {
                    case "0":
                        log.info("Завершение сеанса");
                        System.exit(1);
                    case "1":
                        log.info("Просмотренны все мероприятия");
                        for (Object e : readerEvent.readAllEvent()) {
                            System.out.println(e);
                        }
                        buyTicket();
                        break;
                    case "2":
                        log.info("Открыто меню покупки билетов");
                        buyTicket();
                        break;
                    case "3":
                        log.info("Просмотрены свои билеты");
                        readerTicket.checkMyTickets(readerPerson.readYourName(YOUR_ID));
                        break;
                    case "4":
                        log.info("Открыто меню возврата билета");
                        readerTicket.returnTicked(readerPerson.readYourName(YOUR_ID));
                        break;
                    case "5":
                        log.info("Открыто меню смены пароля");
                        readerPerson.changeMinePassword(YOUR_ID);
                        break;
                    case "9":
                        break;
                    default:
                        System.err.println("Выбран неправильный пункт меню, повторите ввод");
                }
            }
        } catch (Exception e) {
            log.warning("Введены не коректные данные");
            System.out.println("ВЫ ВВЕЛИ НЕ КОРЕКТНЫЕ ДАННЫЕ" + "\n" +
                    "ПОВТОРИТЕ ПОПЫТКУ" + "\n" +
                    "================================================================");
        }
    }

    @Override
    public void buyTicket() {
        try {
            while (true) {

                Scanner sc = new Scanner(System.in);
                System.out.println(" " + "\n" +
                        "======= МЕНЮ ПОКУПКИ ========" + "\n" +
                        " " + "\n" +
                        "- Для возврата к предыдущему меню '0'" + "\n" +
                        "- Введите ID Мероприятия для просмотра доступных мест " + "\n" +
                        "--->");
                String str = sc.nextLine();
                switch (str) {
                    case "0":
                        userMenu();
                        break;
                    default:
                        System.out.println(readerEvent.readEvent(Integer.valueOf(str)));
                        readerTicket.printHall(changeTicket(Integer.valueOf(str)));
                        log.info("Просмотренны свободные места мероприятия: " + Integer.valueOf(str));
                }
                while (true) {
                    System.out.println(" - Для возврата к предыдущему меню введите '0'");
                    System.out.println(" - Для покупки билета введите '1'");
                    String choice2 = sc.nextLine();
                    switch (choice2) {
                        case "0":
                            buyTicket();
                        case "1":
                            readerTicket.buyTicket(Integer.valueOf(choice2), changeTicket(Integer.valueOf(str)),
                                    Integer.valueOf(str), readerPerson.readYourName(YOUR_ID));
                            break;
                        default:
                            System.err.println("Выбран неправильный пункт меню, повторите ввод");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("ВВЕДЕНЫ НЕ КОРЕКТНЫЕ ДАННЫЕ");
            buyTicket();
        }
    }


    @Override
    public void managerMenu() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            log.info("Открыто меню менеджера");
            System.out.println(" " + "\n" +
                    "======= МЕНЮ МЕНЕДЖЕРА ========" + "\n" +
                    " " + "\n" +
                    "Выберете пункт меню:" + "\n" +
                    "1. Для просмотра мероприятий введите '1'" + "\n" +
                    "2. Для покупки билета введите '2'" + "\n" +
                    "3. Для просмотра ваших билетов введите '3'" + "\n" +
                    "4. Для возврата билета введите '4'" + "\n" +
                    "5. Для редактирования мероприятия введите '5'" + "\n" +
                    "6. Для покупки билета другому пользователю введите '6'" + "\n" +
                    "7. Для возврата билета пользователя введите '7'" + "\n" +
                    "8. Для смены своего пароля введите '8'" + "\n" +
                    "9. Для возврата к предыдущему меню '9'" + "\n" +
                    "0. Для выхода из программы '0'" + "\n" +
                    "--->");
            String choice = sc.nextLine();
            switch (choice) {
                case "0":
                    log.info("Завершение сеанса");
                    System.exit(1);
                case "1":
                    log.info("Просмотренны все мероприятия");
                    for (Object e : readerEvent.readAllEvent()) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    log.info("Открыто меню покупки билетов");
                    buyTicket();
                    break;
                case "3":
                    log.info("Просмотрены свои билеты");
                    readerTicket.checkMyTickets(readerPerson.readYourName(YOUR_ID));
                    break;
                case "4":
                    log.info("Открыто меню возврата билета");
                    readerTicket.returnTicked(readerPerson.readYourName(YOUR_ID));
                    break;
                case "5":
                    log.info("Открыто меню редактирования мероприятия");
                    readerEvent.editEvent();
                    break;
                case "6":
                    log.info("Открыто меню покипку билета для другого пользователя");
                    readerTicket.buyTicketForUser();
                    break;
                case "7":
                    log.info("Открыто меню возвтара билета другого пользователя");
                    readerTicket.returnTickedForUser();
                    break;
                case "8":
                    log.info("Открыто меню смены пароля");
                    readerPerson.changeMinePassword(YOUR_ID);
                    break;
                case "9":
                    break;
                default:
                    System.err.println("Выбран неправильный пункт меню, повторите ввод");
            }
        }
    }

    @Override
    public void adminMenu() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            log.info("Открыто меню администратора");
            System.out.println(" " + "\n" +
                    "======= МЕНЮ АДМИНИСТРАТОРА ========" + "\n" +
                    " " + "\n" +
                    "Выберете пункт меню:" + "\n" +
                    "1. Для вызова меню Пользователей введите '1'" + "\n" +
                    "2. Для вызова меню Мероприятий введите '2'" + "\n" +
                    "3. Для вызова меню Билетов введите  '3'" + "\n" +
                    "4. Для очистки БД от устаревшей информации введите '4'" + "\n" +
                    "9. Для возврата к предыдущему меню '9'" + "\n" +
                    "0. Для выхода из программы '0'" + "\n" +
                    "--->");
            String choice = sc.nextLine();
            switch (choice) {
                case "0":
                    log.info("Завершение сеанса");
                    System.exit(1);
                case "1":
                    editUser();
                    break;
                case "2":
                    editFilm();
                    break;
                case "3":
                    editTicket();
                    break;
                case "4":
                    List<Integer> films = readerEvent.readOldEvent();
                    readerTicket.removeOldTickets(films);
                    readerEvent.removeOldEvent(films);
                    break;
                case "9":
                    break;
                default:
                    System.err.println("Выбран неправильный пункт меню, повторите ввод");
            }
        }
    }

    @Override
    public void editFilm() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            log.info("Открыто меню редактирования мероприятий");
            System.out.println(" " + "\n" +
                    "==== РЕДАКТИРОВАНИЕ МЕРОПРИЯТИЙ (АДМИН) ====" + "\n" +
                    " " + "\n" +
                    "Выберете пункт меню:" + "\n" +
                    "1. Для просмотра мероприятий введите '1'" + "\n" +
                    "2. Для редактирования мероприятия введите '2'" + "\n" +
                    "3. Для удаления мероприятия введите '3'" + "\n" +
                    "4. Для создания мероприятия введите '4'" + "\n" +
                    "9. Для возврата к предыдущему меню '9'" + "\n" +
                    "0. Для выхода из программы '0'" + "\n" +
                    "--->");
            String choice = sc.nextLine();
            switch (choice) {
                case "0":
                    log.info("Завершение сеанса");
                    System.exit(1);
                case "1":
                    log.info("Просмотренны все мероприятия");
                    for (Object e : readerEvent.readAllEvent()) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    log.info("Открыто меню редактирования мероприятия");
                    readerEvent.editEvent();
                    break;
                case "3":
                    log.info("Открыто меню удаления мероприятия");
                    readerEvent.removeEvent();
                    break;
                case "4":
                    log.info("Открыто меню создания мероприятия");
                    readerEvent.createEvent();
                    break;
                case "9":
                    adminMenu();
                    break;
                default:
                    System.err.println("Выбран неправильный пункт меню, повторите ввод");
            }
        }
    }

    @Override
    public void editUser() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            log.info("Открыто меню редактирования пользователя");
            System.out.println(" " + "\n" +
                    "==== РЕДАКТИРОВАНИЕ ПОЛЬЗОВАТЕЛЕЙ (АДМИН) ====" + "\n" +
                    " " + "\n" +
                    "Выберете пункт меню:" + "\n" +
                    "1. Для создания пользователя введите '1'" + "\n" +
                    "2. Для изменение пароля пользователю введите '2'" + "\n" +
                    "3. Для изменение логина пользователю введите '3'" + "\n" +
                    "4. Для удаления пользователя введите '4'" + "\n" +
                    "5. Для смены своего пароля введите '5'" + "\n" +
                    "9. Для возврата к предыдущему меню '9'" + "\n" +
                    "0. Для выхода из программы '0'" + "\n" +
                    "--->");
            String choice = sc.nextLine();
            switch (choice) {
                case "0":
                    log.info("Завершение сеанса");
                    System.exit(1);
                case "1":
                    log.info("Открыто меню регистрации пользователя");
                    readerPerson.registrationUser(readerPerson.registrationLoginUser(),
                            readerPerson.registrationPasswordUser());
                    break;
                case "2":
                    log.info("Открыто меню смены пароля пользователя");
                    readerPerson.changePasswordForUser();
                    break;
                case "3":
                    log.info("Открыто меню смены логина пользователя");
                    readerPerson.changeLoginForUser();
                    break;
                case "4":
                    log.info("Открыто меню удаления пользователя");
                    readerPerson.removeUser();
                    break;
                case "5":
                    log.info("Открыто меню смены пароля");
                    readerPerson.changeMinePassword(YOUR_ID);
                    break;
                case "9":
                    adminMenu();
                    break;
                default:
                    System.err.println("Выбран неправильный пункт меню, повторите ввод");
            }
        }
    }

    @Override
    public void editTicket() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            log.info("Открыто меню редактирования билетов");
            System.out.println(" " + "\n" +
                    "==== РЕДАКТИРОВАНИЕ БИЛЕТОВ (АДМИН) ====" + "\n" +
                    " " + "\n" +
                    "Выберете пункт меню:" + "\n" +
                    "1. Для просмотра мероприятий введите '1'" + "\n" +
                    "2. Для покупки билета введите '2'" + "\n" +
                    "3. Для просмотра ваших билетов введите '3'" + "\n" +
                    "4. Для возврата билета введите '4'" + "\n" +
                    "5. Для покупки билета другому пользователю введите '5'" + "\n" +
                    "6. Для возврата билета пользователя введите '6'" + "\n" +
                    "9. Для возврата к предыдущему меню '9'" + "\n" +
                    "0. Для выхода из программы '0'" + "\n" +
                    "--->");
            String choice = sc.nextLine();
            switch (choice) {
                case "0":
                    log.info("Завершение сеанса");
                    System.exit(1);
                case "1":
                    log.info("Просмотренны все мероприятия");
                    for (Object e : readerEvent.readAllEvent()) {
                        System.out.println(e);
                    }
                    break;
                case "2":
                    log.info("Открыто меню покупки билетов");
                    buyTicket();
                    break;
                case "3":
                    log.info("Просмотрены свои билеты");
                    readerTicket.checkMyTickets(readerPerson.readYourName(YOUR_ID));
                    break;
                case "4":
                    log.info("Открыто меню возврата билета");
                    readerTicket.returnTicked(readerPerson.readYourName(YOUR_ID));
                    break;
                case "5":
                    log.info("Открыто меню покипку билета для другого пользователя");
                    readerTicket.buyTicketForUser();
                    break;
                case "6":
                    log.info("Открыто меню возвтара билета другого пользователя");
                    readerTicket.returnTickedForUser();
                    break;
                case "9":
                    adminMenu();
                    break;
                default:
                    System.err.println("Выбран неправильный пункт меню, повторите ввод");
            }
        }
    }

    public void comparisonUsers(Person firstUser, Person secondUser) {
        if (readerPerson.comparisonPassword(firstUser, secondUser)) {
            System.out.println("Вы ввели ВЕРНЫЙ ПАРОЛЬ ");
            log.info("Вход пользователя: " + secondUser.getLogin());
            if (secondUser.getRole() == 2) {
                managerMenu();
            }
            if (secondUser.getRole() == 3) {
                adminMenu();
            } else userMenu();
        } else {
            log.warning("Введен не верный пароль для пользователя: " + secondUser.getLogin());
            System.out.println("НЕ ВЕРНЫЙ ПАРОЛЬ" + "\n" +
                    "ПОВТОРИТЕ ПОПЫТКУ" + "\n" +
                    "==================================================================");
        }
    }

    public int[][] changeTicket(int choice) {
        Event event = readerEvent.readEvent(choice);
        if (event == null) {
            System.out.println("Данного мероприятия не существует");
            System.out.println("Введите коректные данные");
            buyTicket();
        }
        int[][] hall = readerTicket.choiceTicket(choice);
        return hall;
    }
}