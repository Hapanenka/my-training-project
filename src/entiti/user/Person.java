package entiti.user;

public abstract class Person {
    private Integer id;
    private String login;
    private String password;
    private int role;

    public Person(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Person(String login, String password, int role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public Person(Integer id, String login) {
        this.id = id;
        this.login = login;
    }

    public Person(Integer id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Person() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}