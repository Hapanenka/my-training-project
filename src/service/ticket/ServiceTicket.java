package service.ticket;

import java.util.List;

public interface ServiceTicket<T> {

    void buyTicket(Integer y, int[][] x, int z, String s);

    int[][] choiceTicket(Integer x);

    void printHall(int[][] x);

    void checkMyTickets(String s);

    boolean returnTicked(String x);

    void buyTicketForUser();

    void returnTickedForUser();

    void removeOldTickets(List<Integer> x);
}


