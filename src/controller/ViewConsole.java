package controller;

public interface ViewConsole {
    int start();

    void userMenu();

    void adminMenu();

    void managerMenu();

    void editFilm();

    void editUser();

    void buyTicket();

    void editTicket();
}