import controller.Console;
import controller.ViewConsole;
import repository.event.EventRepositoryImpl;
import repository.person.PersonRepositoryImpl;
import repository.ticket.TicketRepositoryImpl;
import service.event.ServiceEventImpl;
import service.person.ServicePersonImpl;
import service.ticket.ServiceTicketImpl;

import java.util.logging.*;

public class Main {

    public static final Logger log = Logger.getLogger(Main.class.getSimpleName());

    public static void main(String[] args) {
        log.info("Старт программы");

        EventRepositoryImpl eventRepository = new EventRepositoryImpl();
        PersonRepositoryImpl personRepository = new PersonRepositoryImpl();
        TicketRepositoryImpl ticketRepository = new TicketRepositoryImpl();

        ServiceEventImpl serviceEvent = new ServiceEventImpl(eventRepository);
        ServicePersonImpl servicePerson = new ServicePersonImpl(personRepository);
        ServiceTicketImpl serviceTicket = new ServiceTicketImpl(ticketRepository);

        ViewConsole viewConsole = new Console(servicePerson, serviceEvent, serviceTicket);

        viewConsole.start();

    }
} 