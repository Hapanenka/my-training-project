package service.ticket;

import entiti.ticket.Ticket;
import repository.ticket.TicketRepository;

import java.util.List;

public class ServiceTicketImpl implements ServiceTicket<Ticket> {

    private TicketRepository<Ticket> ticketRepository;

    public ServiceTicketImpl(TicketRepository<Ticket> ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public void buyTicket(Integer y, int[][] x, int z, String s) {
        try {
            ticketRepository.buy(y, x, z, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int[][] choiceTicket(Integer x) {
        try {
            return ticketRepository.choice(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void printHall(int[][] x) {
        try {
            ticketRepository.print(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkMyTickets(String s) {
        try {
            ticketRepository.checkMy(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean returnTicked(String x) {
        try {
            return ticketRepository.returnT(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void buyTicketForUser() {
        try {
            ticketRepository.buyForUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void returnTickedForUser() {
        try {
            ticketRepository.returnForUser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeOldTickets(List<Integer> x) {
        try {
            ticketRepository.removeOld(x);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
