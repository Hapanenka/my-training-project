package service.event;

import entiti.event.Event;
import repository.event.EventRepository;

import java.util.List;

public class ServiceEventImpl implements ServiceEvent<Event> {

    private EventRepository<Event> eventRepository;

    public ServiceEventImpl(EventRepository<Event> eventRepository) {
        this.eventRepository = eventRepository;
    }


    public List readAllEvent(){
        try {
            return eventRepository.readAll();
        }catch (Exception e){
            return null;
        }
    }

    public List readOldEvent() {
        try {
            return eventRepository.readOld();
        }catch (Exception e){
            return null;
        }
    }


    public void removeOldEvent(List<Integer> x){
        try {
            eventRepository.removeOld(x);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean editEvent(){
        try {
            return eventRepository.edit();
        }catch (Exception e){
            return false;
        }
    }

    public boolean createEvent(){
        try {
            return eventRepository.create();
        }catch (Exception e){
            return false;
        }
    }


    public Event readEvent(int x) {
        try {
            return eventRepository.read(x);
        }catch (Exception e){
            return null;
        }
    }

    public boolean removeEvent(){
        try {
            return eventRepository.remove();
        }catch (Exception e){
            return false;
        }
    }

}
