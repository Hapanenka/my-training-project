package entiti.user;

import entiti.ticket.Ticket;

public class Manager extends Person{

    public Manager(String login, String password) {
        super(login, password);
    }

    public Manager(Integer id, String login) {
        super(id, login);
    }

    public Manager(Integer id, String login, String password) {
        super(id, login, password);
    }

}