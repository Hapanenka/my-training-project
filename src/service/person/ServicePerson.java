package service.person;

public interface ServicePerson<T> {

    String registrationLoginUser();

    String registrationPasswordUser();

    T enterUser();

    void registrationUser(String t, String e);

    boolean removeUser();

    T readUserFromDB(T t);

    void changePasswordForUser();

    void changeMinePassword(int i);

    void changeLoginForUser();

    int readYourIdSession(T t);

    String readYourName(int x);

    boolean comparisonPassword(T t, T k);

}
