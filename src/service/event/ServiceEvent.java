package service.event;

import entiti.event.Event;

import java.util.List;

public interface ServiceEvent<T> {

    List<T> readAllEvent();

    boolean editEvent();

    boolean createEvent();

    Event readEvent(int x);

    boolean removeEvent();

    void removeOldEvent(List<Integer> x);

    List<T> readOldEvent();


}
