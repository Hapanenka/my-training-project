package repository.person;



public interface PersonRepository<T> {

    String registrationLogin();

    String registrationPassword();

    T enter();

    void registration(String t, String e);

    boolean remove();

    T readFromDB(T t);

    void changePasswordFor();

    void changePassword(int i);

    void changeLoginFor();

    int readIdSession(T t);

    String readYour(int x);

    boolean checkPassword(T t, T k);
}