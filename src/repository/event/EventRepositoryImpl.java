package repository.event;

import entiti.event.Event;
import util.ConnectionManager;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class EventRepositoryImpl implements EventRepository<Event> {

    public static final Logger log = Logger.getLogger(EventRepositoryImpl.class.getName());


     @Override
    public List readAll() {
        List<Event> films = new ArrayList<>();
        Event film;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT * FROM films";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Date date = resultSet.getDate(3);
                Time time = resultSet.getTime(4);
                film = new Event(id, name, date, time);
                films.add(film);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return films;
    }

    @Override
    public List readOld() {
        List<Integer> oldFilm = new ArrayList<>();
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT id, date FROM films";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            Date yesterday = Date.valueOf(LocalDate.now().minusDays(1));
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                Date date = resultSet.getDate(2);
                if (yesterday.after(date)) {
                    oldFilm.add(id);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oldFilm;
    }


    @Override
    public void removeOld(List<Integer> x) {
        for (Integer idFilm : x) {
            try (Connection connection = ConnectionManager.open()) {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM films WHERE id=?");
                statement.setInt(1, idFilm);
                statement.executeUpdate();
                log.warning("УДАЛЕНА НЕ АКТУАЛЬНАЯ ИНФОРМАЦИЯ О МЕРОПРИЯТИЯХ");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean edit() {
         List<Event> event = readAll();
        for (Event e:event) {
            System.out.println(e);
        }
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Введите Id для изменения названия: ");
            String str = sc.nextLine();
            int id = Integer.valueOf(str);
            System.out.println("Введите Новое название");
            String newName = sc.nextLine();
            try (Connection connection = ConnectionManager.open()) {
                String sql = "UPDATE films SET name=? WHERE id=?";
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setString(1, newName);
                statement.setInt(2, id);
                statement.execute();
                log.info("Редактированно мероприятие" + id+ "новое название" + newName);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    @Override
    public boolean create() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Название фильма(мероприятия)");
        final String name = sc.nextLine();
        System.out.println("Введите дату начала в формате гггг-ММ-дд");
        final String date = sc.nextLine();
        System.out.println("Введите время начала мероприятия в формате HH:mm:ss");
        final String time = sc.nextLine();
        try (Connection conn = ConnectionManager.open()) {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO films (name, date, time) VALUES (?,?,?)");
            stmt.setString(1, name);
            stmt.setDate(2, Date.valueOf(date));
            stmt.setTime(3, Time.valueOf(time));
            stmt.execute();
            System.out.println("Мероприятие '" + name + " " + date + " "
                    + time + "' успешно добавлено");
            log.info("Мероприятие '" + name + " " + date + " "
                    + time + "' успешно добавлено");
        } catch (Exception e) {
            System.err.println("ОШИБКА ДОБАВЛЕНИЯ МЕРОПРИЯТИЯ");
        }
        return false;
    }

    @Override
    public Event read(int x) {
        Event event = null;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT * FROM films WHERE id=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, x);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                Date date = resultSet.getDate("date");
                Time time = resultSet.getTime("time");
                event = new Event(x, name, date, time);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return event;
    }

    @Override
    public boolean remove() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ID мероприятия для УДАЛЕНИЯ");
        int id = sc.nextInt();
        try (Connection connection = ConnectionManager.open()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM films WHERE id=?");
            statement.setInt(1, id);
            int x = statement.executeUpdate();
            if (x > 0) {
                System.out.println("Мероприятие с ID: " + id + " Успешно удалено");
                log.warning("Удалено мероприятие: " + id);
                return true;
            } else System.err.println("Мероприятие не найдено");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}