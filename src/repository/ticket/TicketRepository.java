package repository.ticket;


import java.util.List;


public interface TicketRepository<T> {

    void buy(Integer y, int[][] x, int z, String s);

    int[][] choice(Integer x);

    void print(int[][] x);

    void checkMy(String s);

    boolean returnT(String x);

    void buyForUser();

    void returnForUser();

    void removeOld(List<Integer> x);

}