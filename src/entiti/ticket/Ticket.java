package entiti.ticket;

import entiti.event.Event;

public class Ticket {
    private Integer id;
    private String person;
    private Event event;
    private int row;
    private int place;
    private Integer cost;

    public Ticket(Integer id, String person, Event event, int row, int place) {
        this.id = id;
        this.person = person;
        this.event = event;
        this.row = row;
        this.place = place;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public Ticket() {
    }

    @Override
    public String toString() {
        return "Билет: " +
                "ID= " + id +
                ", посетитель= " + person +
                ", Сеанс= " + event +
                ", Ряд= " + row +
                ", Место= " + place;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return person;
    }

    public void setUser(String person) {
        this.person = person;
    }

    public Event getFilm() {
        return event;
    }

    public void setFilm(Event event) {
        this.event = event;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}