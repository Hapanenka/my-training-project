package repository.person;

import entiti.user.Person;
import entiti.user.User;
import util.ConnectionManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonRepositoryImpl implements PersonRepository<Person> {

    public static final Logger log = Logger.getLogger(PersonRepositoryImpl.class.getSimpleName());

    @Override
    public User enter() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Логин");
        final String login = sc.nextLine();
        System.out.println("Введите пароль");
        final String password = sc.nextLine();
        User user = new User(login, password);
        return user;
    }

    @Override
    public String registrationLogin() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите желаемый Логин для проверки соответствия: ");
        final String login = sc.nextLine();
        if (login.length() < 5) {
            System.out.println("Вы ввели слишком короткий логин. Должен быть 5 и более символов.");
            registrationLogin();
        }
        int yourId = 0;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT id FROM users WHERE login=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                yourId = resultSet.getInt("id");
                if (yourId != 0) {
                    System.out.println("Логин для регистрации не доступен, быверите другой: ");
                    registrationLogin();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return login;
    }

    @Override
    public String registrationPassword() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите пароль (больше 5ти но не больше 20 символов, " +
                "содержащий минимум 1 лат.букву, 1 цифру и 1 спецсимвол): ");
        final String password = sc.nextLine();
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-zA-Z])"
                + "(?=.*[@#$%*^&+=])"
                + "(?=\\S+$).{5,20}$";
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(password);
        boolean found = matcher.find();
        if (!found) {
            System.out.println("Вы ввели не коректный пароль. Повторите Ввод: ");
            registrationPassword();
        }
        return password;
    }

    @Override
    public void registration(String login, String password) {
        try (Connection conn = ConnectionManager.open()) {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (login, password) VALUES (?,?)");
            stmt.setString(1, login);
            stmt.setString(2, md5Custom(password));
            stmt.execute();
            System.out.println("Пользователь '" + login + "' успешно добавлен");
            log.info("Зарегестрирован новый пользователь: " + login);
        } catch (SQLException e) {
            System.err.println("ОШИБКА АВТОРИЗАЦИИ");
            log.warning("Ошибка регистрации пользователя");
        }
    }

    @Override
    public void changePassword(int i) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Старый Пароль");
        final String oldPassword = md5Custom(sc.nextLine());
        System.out.println("Введите Новый Пароль");
        final String password = md5Custom(registrationPassword());
        try (Connection connection = ConnectionManager.open()) {
            String sql = "UPDATE users SET password=? WHERE id=? and password=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, password);
            stmt.setInt(2, i);
            stmt.setString(3, oldPassword);
            stmt.execute();
        } catch (SQLException e) {
            System.err.println("Не удачная смена пароля");
            log.info("Не удачная смена пароля пользователю с ID: " + i);
        }
    }

    @Override
    public void changePasswordFor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Логин Пользователя");
        final String login = sc.nextLine();
        System.out.println("Введите Новый Пароль");
        final String password = md5Custom(registrationPassword());
        try (Connection connection = ConnectionManager.open()) {
            String sql = "UPDATE users SET password=? WHERE login=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, password);
            stmt.setString(2, login);
            stmt.execute();
            log.info("Сменен пароль пользователю: " + login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeLoginFor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Логин Пользователя для изменения");
        final String loginOld = sc.nextLine();
        System.out.println("Введите Новый Логин");
        final String loginNew = registrationLogin();
        try (Connection connection = ConnectionManager.open()) {
            String sql = "UPDATE users SET login=? WHERE login=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, loginNew);
            stmt.setString(2, loginOld);
            stmt.execute();
            log.info("Сменен логин пользователю: " + loginOld + " на логин " + loginNew);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Person readFromDB(Person person) {
        User userFound = null;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT id, login, password, role FROM users WHERE login=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, person.getLogin());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int yourId = resultSet.getInt("id");
                String username = resultSet.getString("login");
                String password_2 = resultSet.getString("password");
                Integer role = resultSet.getInt("role");
                userFound = new User(yourId, username, password_2, role);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userFound;
    }

    @Override
    public int readIdSession(Person person) {
        int yourId = 0;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT id FROM users WHERE login=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, person.getLogin());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                yourId = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return yourId;
    }

    @Override
    public String readYour(int x) {
        String login = null;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT login FROM users WHERE id=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, x);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                login = resultSet.getString("login");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return login;
    }

    @Override
    public boolean checkPassword(Person t, Person k) {

        return md5Custom(t.getPassword()).equals(k.getPassword());
    }

    @Override
    public boolean remove() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Логин для удаления из БД");
        final String login = sc.nextLine();
        try (Connection connection = ConnectionManager.open()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE login=?");
            statement.setString(1, login);
            int x = statement.executeUpdate();
            if (x > 0) {
                log.warning("Удален пользователь: " + login);
                System.err.println(login + " Успешно удален");
                return true;
            } else System.err.println("Пользователь не найден");

        } catch (SQLException e) {
            System.err.println("Пользователя удалить нельзя т.к. у него имеются зарегестрированные билеты");
        }
        return false;
    }

    public static String md5Custom(String st) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }
        return md5Hex;
    }
}