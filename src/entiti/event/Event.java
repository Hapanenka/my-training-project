package entiti.event;

import java.sql.Time;
import java.util.Date;

public class Event {
    private Integer id;
    private String name;
    private Date date;
    private Time time;

    public Event(Integer id, String name, Date date, Time time) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
    }

    @Override
    public String toString() {
        return "ID= " + id +
                ", Фильм = '" + name + '\'' +
                ", Дата = " + date +
                ", Время начала = " + time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}