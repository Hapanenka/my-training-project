package repository.ticket;

import entiti.event.Event;
import entiti.ticket.Ticket;
import entiti.user.User;
import util.ConnectionManager;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class TicketRepositoryImpl implements TicketRepository<Ticket> {

    public static Logger log = Logger.getLogger(TicketRepositoryImpl.class.getName());


    @Override
    public int[][] choice(Integer x) {
        int[][] hall = new int[8][9];
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT row, place FROM tickets WHERE id_film=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, x);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Integer row = resultSet.getInt("row");
                Integer place = resultSet.getInt("place");
                hall[row - 1][place - 1] = 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hall;
    }

    @Override
    public void removeOld(List<Integer> x) {
        for (Integer idFilm : x) {
            try (Connection connection = ConnectionManager.open()) {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM tickets WHERE id_film=?");
                statement.setInt(1, idFilm);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public int changeRow() {
        try {
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Введите выбранный ряд");
            String str = sc.nextLine();
            int row = Integer.valueOf(str);
            if (row > 0 && row < 9) {
                return row;
            } else {
                System.err.println("В КИНОЗАЛЕ 8 РЯДОВ, ВВЕДИТЕ КОРРЕКТНЫЕ ДАННЫЕ");
                log.warning("Не удачная покупка выбора ряда");
            }
        }
            } catch (Exception e) {
            System.err.println("ВВЕДИТЕ КОРРЕКТНЫЕ ДАННЫЕ");
            log.warning("Не удачная покупка выбора ряда");
        }return changeRow();
    }

    public int changePlace() {
        try {
            while (true) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Введите выбранное место");
                String str = sc.nextLine();
                int place = Integer.valueOf(str);
                if (place > 0 && place < 10) {
                    return place;
                } else {
                    System.err.println("В КИНОЗАЛЕ 9 МЕСТ В РЯДУ, ВВЕДИТЕ КОРРЕКТНЫЕ ДАННЫЕ");
                    log.warning("Не удачная покупка выбора места");
                }
            }
        } catch (Exception e) {
            System.err.println("ВВЕДИТЕ КОРРЕКТНЫЕ ДАННЫЕ");
            log.warning("Не удачная покупка выбора места");
        }return changePlace();
    }

    @Override
    public void buy(Integer y, int[][] hall, int id_film, String login) {
        while (y > 0) {
            int row = changeRow();
            int place = changePlace();
            if (hall[row - 1][place - 1] == 1) {
                System.out.println("Это место занято выбирите другое");
            }
            if (hall[row - 1][place - 1] == 0) {
                hall[row - 1][place - 1] = 1;
                try (Connection conn = ConnectionManager.open()) {
                    PreparedStatement stmt = conn.prepareStatement(
                            "INSERT INTO tickets (id_film, row, place, user) VALUES (?,?,?,?)");
                    stmt.setInt(1, id_film);
                    stmt.setInt(2, row);
                    stmt.setInt(3, place);
                    stmt.setString(4, login);
                    stmt.execute();
                    log.info("Куплен билет на мероприятие №: " + id_film +
                            " ряд " + row + " место " + place + " пользователь " + login);
                    System.out.println("БИЛЕТ ЗАРЕГЕСТРИРОВАН/КУПЛЕН");
                } catch (SQLException e) {
                    log.warning("Не удачная покупка билета");
                    System.err.println("ОШИБКА ПОКУПКИ");
                }
                y--;
            } else {
                System.out.println("Вы ввели не коректные данные");
                log.warning("Не удачная покупка билета");
            }
        }
    }

    public Event readEvent(int x) {
        Event event = null;
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT * FROM films WHERE id=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, x);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                Date date = resultSet.getDate("date");
                Time time = resultSet.getTime("time");
                event = new Event(x, name, date, time);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return event;
    }

    @Override
    public void checkMy(String login) {
        try (Connection connection = ConnectionManager.open()) {
            String sql = "SELECT id, id_film, row, place FROM tickets WHERE user=?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int id_film = resultSet.getInt("id_film");
                int row = resultSet.getInt("row");
                int place = resultSet.getInt("place");
                Ticket ticket = new Ticket(id, login, readEvent(id_film), row, place);
                System.out.println(ticket);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void print(int[][] hall) {
        System.out.println("=======================================");
        System.out.println("       Э К Р А Н");
        System.out.println("  ---------------------");
        for (int i = 0; i < hall.length; i++) {
            System.out.print((i + 1) + " | ");
            for (int j = 0; j < hall[0].length; j++) {
                System.out.print(hall[i][j] + " ");
            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println("  ---------------------");
        System.out.println("    1 2 3 4 5 6 7 8 9");
        System.out.println("=======================================");
    }

    @Override
    public boolean returnT(String name) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ID своего билета для возврата: ");
        int id = sc.nextInt();
        try (Connection connection = ConnectionManager.open()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM tickets WHERE id=? and user=?");
            statement.setInt(1, id);
            statement.setString(2, name);
            statement.executeUpdate();
            System.err.println("ВАШ БИЛЕТ № " + id + " ВОЗВРАЩЕН");
            log.info("Возвращен билет " + id + " пользователем: " + name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void buyForUser() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите LOGIN пользователя кому покупаете билет: ");
        String login = sc.nextLine();
        System.out.println("Введите ID мероприятия: ");
        int event = sc.nextInt();
        int hall[][] = choice(event);
        int row = changeRow();
        int place = changePlace();
        if (hall[row - 1][place - 1] == 1) {
            System.out.println("Это место занято выбирите другое");
        }
        if (hall[row - 1][place - 1] == 0) {
            hall[row - 1][place - 1] = 1;
            try (Connection conn = ConnectionManager.open()) {
                PreparedStatement stmt = conn.prepareStatement(
                        "INSERT INTO tickets (id_film, row, place, user) VALUES (?,?,?,?)");
                stmt.setInt(1, event);
                stmt.setInt(2, row);
                stmt.setInt(3, place);
                stmt.setString(4, login);
                stmt.execute();
                System.out.println("БИЛЕТ ЗАРЕГЕСТРИРОВАН/КУПЛЕН");
                log.info("Куплен билет на мероприятие:" + event + " ряд:" + row + " место:" + place + " для пользователя: " + login);
            } catch (SQLException e) {
                System.err.println("ОШИБКА ПОКУПКИ. ВВЕДЕНЫ НЕ КОРЕКТНЫЕ ДАННЫЕ");
            }
        } else System.out.println("Вы ввели не коректные данные");
    }

    @Override
    public void returnForUser() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ID билета для возврата: ");
        int id = sc.nextInt();
        try (Connection connection = ConnectionManager.open()) {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM tickets WHERE id=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            log.info("Возвращен билет " + id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}